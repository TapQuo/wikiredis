"use strict"

Yoi       = require "yoi"

module.exports = ->

  session = test.users[0]

  tasks = []
  tasks.push _createStats(user, test.posts[0]) for user in test.users
  tasks

# ==========================================================
_createStats = (user, post) -> ->
  Yoi.Test "POST", "api/stats", {shortcut: post.shortcut}, _session(user), "Cuantas visitas tiene un post", null
# ==========================================================

_session = (user) ->
  if user.id?
    authorization: user.id
  else
    null
