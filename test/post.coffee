"use strict"

Yoi       = require "yoi"

module.exports = ->

  session = test.users[0]

  tasks = []
  tasks.push _create(session, post) for post in test.posts
  tasks.push _listPost(session, list) for list in test.lists
  tasks.push _search(session, test.search)
  tasks.push _doFavorite(session, test.posts[0])
  tasks.push _listFavoriteOfUser(session)
  tasks.push _deleteFavorite(session, test.posts[0])
  tasks

# ==========================================================
_create = (user, post) -> ->
  promise = new Yoi.Hope.Promise()
  Yoi.Test "POST", "api/post", post, _session(user), "Create a post with title #{post.title}", null, (response) ->
    post.id = response.id
    promise.done null, response
  promise

_listPost = (user, list) -> ->
  Yoi.Test "GET", "api/posts", {page: list.page}, _session(user), "List all post"

_doFavorite = (user, post) -> ->
  Yoi.Test "POST", "api/post/favorite", {shortcut: post.shortcut}, _session(user), "Do favorite"

_deleteFavorite = (user, post) -> ->
  Yoi.Test "DELETE", "api/post/favorite", {shortcut: post.shortcut}, _session(user), "Delete Favorite"

_listFavoriteOfUser = (user) -> ->
  Yoi.Test "GET", "api/post/favorite", {}, _session(user), "List all favorites of user session."

_search = (user, attributes) -> ->
  Yoi.Test "POST", "api/search", attributes, _session(user), "Search post by '#{attributes.expression}'"

# ==========================================================

_session = (user) ->
  if user.id?
    authorization: user.id
  else
    null
