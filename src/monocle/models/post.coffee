class __Model.Post extends Monocle.Model

  @fields "content", "created_at", "id", "shortcut", "title", "author", "url"
