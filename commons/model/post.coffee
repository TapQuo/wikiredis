"use strict"

Yoi         = require "yoi"
RedisModel  = require "../RedisModel"
TYPE        = RedisModel.TYPES


class Post extends RedisModel

  name        : "post"

  fields:
    shortcut  : type: TYPE.STRING, get: true
    title     : type: TYPE.STRING
    content   : type: TYPE.STRING
    author    : type: TYPE.STRING
    url       : type: TYPE.STRING

  parse: ->
    id         : @model.id
    title      : @model.title
    shortcut   : @model.shortcut
    content    : @model.content
    author     : @model.author
    url        : @model.url
    created_at : new Date(@model.created_at)
    updated_at : new Date(@model.updated_at)


module.exports = Post
