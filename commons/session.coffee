"use strict"

Yoi   = require "yoi"

module.exports = (rest, callback) ->
  promise = new Yoi.Hope.Promise()

  id = rest.session
  if id
    Yoi.Redis.run "hgetall", "user:#{id}", (error, user) ->
      if error
        do rest.unauthorized
      else
        promise.done null, user
        callback.call callback, user if callback?
  else
    do rest.unauthorized

  promise
