"use strict"
Yoi   = require "yoi"

module.exports = (server) ->
  server.post "/signup", (request, response, next) ->
    site = new Yoi.Site request, response
    Yoi.Hope.shield([->
      Yoi.Appnima.signup site.parameter("mail"), site.parameter("password")
    , (error, appnima) ->
      promise = new Yoi.Hope.Promise()
      Yoi.Redis.run "hmset", "user:#{appnima.id}", _parameters(appnima)
      promise.done null, appnima
      promise
    ]).then (error, user) ->
      if error?
        site.redirect "/", null
      else
        console.log site
        site.redirect "/", user.id

  server.post "/login", (request, response, next) ->
    site = new Yoi.Site request, response
    Yoi.Hope.shield([->
      Yoi.Appnima.login site.parameter("mail"), site.parameter("password")
    , (error, appnima) ->
      promise = new Yoi.Hope.Promise()
      Yoi.Redis.run "hmset", "user:#{appnima.id}", _parameters(appnima)
      promise.done null, appnima
      promise
    ]).then (error, user) ->
      if error?
        site.redirect "/", null
      else
        site.redirect "/", user.id

_parameters = (appnima) ->
  user =
    appnima_id    : appnima.id
    mail          : appnima.mail
    name          : if appnima.name? then appnima.name
    username      : appnima.username
    avatar        : appnima.avatar
    access_token  : appnima.access_token
    refresh_token : appnima.refresh_token
    expires_in    : if appnima.expires_in? then appnima.expires_in else null
  user
