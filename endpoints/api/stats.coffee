"use strict"

Yoi       = require "yoi"
PostModel = require "../../commons/model/post"
Session   = require "../../commons/session"

module.exports = (server) ->

  ###
    Stats to view how many people enter in concret post
    @method post
  ###
  server.post "/api/stats", (request, response) ->
    try
      rest = new Yoi.Rest request, response
      Session rest, (user) ->
        post = new PostModel()
        shortcut = rest.parameter "shortcut"
        Yoi.Redis.run "RPUSH", "#{shortcut}:visit", user.mail, (error, result) ->
          rest.run stats: result
    catch error
      rest.exception error.code, error.message
