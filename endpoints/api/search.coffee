"use strict"

Yoi       = require "yoi"
PostModel = require "../../commons/model/post"
Session   = require "../../commons/session"

module.exports = (server) ->

  ###
    Search by regular expression
    @method post
    @param  {string} Regular expression
    @param  {string} Type: 'post', 'user'
  ###
  server.post "/api/search", (request, response) ->
    try
      rest = new Yoi.Rest request, response
      rest.required ["type"]
      Session rest, (user) ->
        post = new PostModel()
        type = rest.parameter "type"
        expression = rest.parameter "expression"

        Yoi.Redis.run "keys", "#{type}*#{expression}*", (error, result) ->
          rest.run search: result


    catch error
      rest.exception error.code, error.message
