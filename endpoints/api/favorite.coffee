"use strict"

Yoi       = require "yoi"
PostModel = require "../../commons/model/post"
Session   = require "../../commons/session"

module.exports = (server) ->

  ###
    Get user's favorite post
    @method get
    @param {user} Session user
  ###
  server.get "/api/post/favorite", (request, response) ->
    try
      rest = new Yoi.Rest request, response

      Session rest, (user) ->
        post = new PostModel()
        post.getFavorites(user.mail).then (error, results) ->
          rest.run favorites: results

    catch error
      rest.exception error.code, error.message

  ###
    Do favorite to post
    @method post
    @param  {string} Post shortcut
  ###
  server.post "/api/post/favorite", (request, response) ->
    try
      rest = new Yoi.Rest request, response
      Session rest, (user) ->
        post = new PostModel()
        Yoi.Hope.shield([ ->
          post.getBy("shortcut", rest.parameter("shortcut"))
        , (error, redis_post) =>
          post.doPostFavorite redis_post.shortcut, "INCR"
        , (error, shortcut) =>
          post.doUserPostFavorite shortcut, user.mail
        ]).then (error, result) ->
          rest.run fav: result
    catch error
      rest.exception error.code, error.message

  ###
    Delete favorite of post
    @method post
    @param  {string} Post shortcut
  ###
  server.del "/api/post/favorite", (request, response) ->
    try
      rest = new Yoi.Rest request, response
      Session rest, (user) ->
        post = new PostModel()
        Yoi.Hope.shield([ ->
          post.getBy("shortcut", rest.parameter("shortcut"))
        , (error, redis_post) =>
          post.doPostFavorite redis_post.shortcut, "DECR"
        , (error, shortcut) =>
          post.deleteFavorite user.mail, shortcut
        ]).then (error, result) ->
          rest.run fav: result
    catch error
      rest.exception error.code, error.message
